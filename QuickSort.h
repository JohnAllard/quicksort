#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

int ChoosePivot(int Array[], int L, int R);
int Partition(int Array[], int L, int r);
int QuickSort(int Array[], int L, int R);
void showarray(int *Array, int n);
int counttotal(int);
void myswap(int Array[], int index1, int index2);




int QuickSort(int *Array,int L,int R)
{

	if(R-L <= 1 || R<L)
		return 0;

	int Length = R-L;
	
	int p;// = Array[L]
	p = Partition(Array, L, R); // partition subroutine returns the integer index of where the pivot is in the array when the partition routine is finished
	
	QuickSort(Array, L, p); // recursively sort array up to the pivot
	counttotal(p-L-1);	// counts the inversions in the left array
	QuickSort(Array, p+1, R);// recursively sort the array after the pivot
	counttotal(R-p); 
 
}

int ChoosePivot(int *Array, int L, int R)
{
	return rand()%(R-L)+L;
}


int Partition(int *Array, int L, int R)
{
	int holdA[3];
	holdA[0] = Array[L];
	holdA[2] = Array[R];
	int index;
	int index1;


	// THSE FOLOWING LINES OF CODE ARE ONLY FOR CHOOSING THE MEDIAN BTWEEN L, R, AND R-L/2
	//if((R-L)%2==0)
	//	{holdA[1] = Array[(R-L)/2]; index1 = (R-L)/2;}
	//else{holdA[1] = Array[(R-L)/2+1]; index1 = (R-L)/2+1;}
	//
	//int hold1 = 2;

	//for(int i = 0; i<3;i++)
	//{
	//	for(int j = i+1; j<3;j++)
	//	{
	//		if(holdA[j] < holdA[i])
	//		{
	//			myswap(holdA,i,j);

	//		}
	//	}
	//}

	//if(holdA[1] == Array[L])
	//{index = L;}
	//else if(holdA[1] == Array[R])
	//	{index = R;}
	//else{index = index1;}
	

	
	
	int pivotindex = ChoosePivot(Array,L,R);
	int hold1 = Array[pivotindex];
	Array[pivotindex] = Array[L];
	Array[L] = hold1;
	
	
	int p = Array[L];
	int i = L+1;
	
	for(int j = i; j < R; j++)
	{
		if(Array[j] < p)
		{
			myswap(Array,i,j);
			i++;
		}
	}

	myswap(Array,L,i-1);

	return i-1;
}




// this function counts the total number of comparisons
int counttotal(int num)
{
	static long unsigned int n = 0;
	n+=num;
	return n;
}

void myswap(int Array[], int index1, int index2)
{
	int hold = Array[index1];
	Array[index1] = Array[index2];
	Array[index2] = hold;

}
